from django.contrib import admin
from .models import Page

# Register your models here to view in admin panel.

admin.site.register(Page)
