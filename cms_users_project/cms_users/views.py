from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login as auth_login

from django.contrib.auth import logout as auth_logout
from .models import Page


# Create your views here.
# Vista para redireccionar a usuarios

html_template = """<!DOCTYPE html>
<html lang="en" >
  <head>
    <meta charset="utf-8" />
    <title>Django CMS</title>
  </head>
  <body>
    {body}
  </body>
</html>
"""

html_item_template = "<li><a href='{name}'>{name}</a></li>"
@login_required
def index(request):

    pages = Page.objects.all()
    if len(pages) == 0:
        body = "No pages yet."
    else:
        body = "<ul>"
        for p in pages:
            body += html_item_template.format(name=p.name)
        body += "</ul>"
    return(HttpResponse(html_template.format(body=body)))


# Primera vista: Vamos a validar las credenciales del usuario
# en caso de ser válidas, se le autenticará
def login(request):
    if request.method == 'POST':
        form = AuthenticationForm(request, request.POST)
        if form.is_valid():
            user = form.get_user()
            auth_login(request, user)
            return redirect('index')  # Redirigir a la página de inicio después de iniciar sesión
    else:
        form = AuthenticationForm()
    return render(request, 'cms_users/login.html', {'form': form})


# Segunda vista: esta vista cerrará la sesión del usuario actual
def logout(request):
    auth_logout(request)
    return redirect('login')  # Redirigir a la página de inicio de sesión después de cerrar sesión



